<?php 
include "config/connect.php";
?>
<html>
<head>
<style>
	img{max-width:100%;}
	body{margin:0;}
	#content img{
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
	}
	#content {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
	}
	.kuning {
		filter: invert(55%) sepia(57%) saturate(2877%) hue-rotate(42deg) brightness(185%) contrast(130%);
	}
	.merah {
		filter:  invert(50%) sepia(90%) saturate(3000%) hue-rotate(-30deg) brightness(104%) contrast(200%);
	}
	#number-image{position:absolute;top:0;left:0;width:100%;z-index:2}
#time {
    position: absolute;
    left: 45%;
    top: 17px;
    font-weight: bold;
    font-size: 30pt;
    border-radius: 30pt;
    height: 68px;
    width: 117px;
    text-align: center;
    background-color: darkolivegreen;
    color: white;
    line-height: 52pt;
}
</style>
</head>
<body>
<span id="time">00:00</span>
<img id="bg-image" src="assets/background.jpg">
<img id="number-image" src="assets/angka-pohon.png">
<div id="content">
	<img src="assets/01.svg" id="block01" class="image-block">
	<img src="assets/02.svg" id="block02" class="image-block">
	<img src="assets/03.svg" id="block03" class="image-block">
	<img src="assets/05.svg" id="block05" class="image-block">
	<img src="assets/06.svg" id="block06" class="image-block">
	<img src="assets/07.svg" id="block07" class="image-block">
	<img src="assets/08.svg" id="block08" class="image-block">
	<img src="assets/09.svg" id="block09" class="image-block">
	<img src="assets/10.svg" id="block10" class="image-block">
	<img src="assets/11.svg" id="block11" class="image-block">
	<img src="assets/12.svg" id="block12" class="image-block">
	<img src="assets/15.svg" id="block15" class="image-block">
	<img src="assets/16.svg" id="block16" class="image-block">
	<img src="assets/17.svg" id="block17" class="image-block">
	<img src="assets/18.svg" id="block18" class="image-block">
	<img src="assets/19.svg" id="block19" class="image-block">
	<img src="assets/20.svg" id="block20" class="image-block">
	<img src="assets/21.svg" id="block21" class="image-block">
	<img src="assets/23.svg" id="block23" class="image-block">
	<img src="assets/25.svg" id="block25" class="image-block">
	<img src="assets/26.svg" id="block26" class="image-block">
	<img src="assets/27.svg" id="block27" class="image-block">
	<img src="assets/28.svg" id="block28" class="image-block">
	<img src="assets/29.svg" id="block29" class="image-block">
	<img src="assets/30.svg" id="block30" class="image-block">
	<img src="assets/32.svg" id="block32" class="image-block">
	<img src="assets/33.svg" id="block33" class="image-block">
	<img src="assets/35.svg" id="block35" class="image-block">
	<img src="assets/36.svg" id="block36" class="image-block">
	<img src="assets/37.svg" id="block37" class="image-block">
	<img src="assets/38.svg" id="block38" class="image-block">
	<img src="assets/39.svg" id="block39" class="image-block">
	<img src="assets/50.svg" id="block50" class="image-block">
	<img src="assets/51.svg" id="block51" class="image-block">
	<img src="assets/52.svg" id="block52" class="image-block">
	<img src="assets/53.svg" id="block53" class="image-block">
	<img src="assets/55.svg" id="block55" class="image-block">
	<img src="assets/56.svg" id="block56" class="image-block">
	<img src="assets/57.svg" id="block57" class="image-block">
	<img src="assets/58.svg" id="block58" class="image-block">
	<img src="assets/59.svg" id="block59" class="image-block">
	<img src="assets/60.svg" id="block60" class="image-block">
	<img src="assets/61.svg" id="block61" class="image-block">
	<img src="assets/62.svg" id="block62" class="image-block">
	<img src="assets/63.svg" id="block63" class="image-block">
	<img src="assets/65.svg" id="block65" class="image-block">
	<img src="assets/66.svg" id="block66" class="image-block">
	<img src="assets/67.svg" id="block67" class="image-block">
	<img src="assets/68.svg" id="block68" class="image-block">
	<img src="assets/69.svg" id="block69" class="image-block">
	<img src="assets/70.svg" id="block70" class="image-block">
	<img src="assets/71.svg" id="block71" class="image-block">
	<img src="assets/72.svg" id="block72" class="image-block">
	<img src="assets/73.svg" id="block73" class="image-block">
	<img src="assets/75.svg" id="block75" class="image-block">
	<img src="assets/76.svg" id="block76" class="image-block">
	<img src="assets/77.svg" id="block77" class="image-block">
	<img src="assets/78.svg" id="block78" class="image-block">
	<img src="assets/79.svg" id="block79" class="image-block">
	<img src="assets/80.svg" id="block80" class="image-block">
	<img src="assets/81.svg" id="block81" class="image-block">
	<img src="assets/82.svg" id="block82" class="image-block">
	<img src="assets/83.svg" id="block83" class="image-block">
</div>
<script src="assets/jquery-1.9.1.js"></script>
<script type="text/javascript" src="assets/shortcut.js"></script>
<script>
var interval;
var last_second=0;
function startTimer(duration, display) {
   var timer = duration;
    interval=setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
			clearInterval(interval);		 
			display.textContent ='00:00';
        }
    }, 1000);
}


function cek_berkala() {
	var placeholder = [];
	
	$.getJSON( "ajax.php", function( data ) {
	  var items = [];
	  $.each( data, function( key, val ) {
		 // console.log(key);
		 // console.log(val);
		$("#block"+key).removeClass("kuning");
		$("#block"+key).removeClass("merah");
		if(val==null || val.length<=0) {
			
		} else {
			$("#block"+key).addClass(val);
		}
	  });
	 
	  
	setTimeout(function(){
		cek_berkala();
	},2000);
	
	});
	
	
}
$(document).ready(function(){
	cek_berkala();
	
	display = document.querySelector('#time');
	shortcut.add("Right",function() {
		clearInterval(interval);
		var Minutes = 60 * 2;
		startTimer(Minutes, display);
			 
		});
		});
	shortcut.add("Left",function() {
		clearInterval(interval);	
		display.textContent ='00:00';		
});
</script>
</body>
</html>